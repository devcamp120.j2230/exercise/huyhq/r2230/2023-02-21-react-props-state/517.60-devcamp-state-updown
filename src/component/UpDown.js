import { Component } from "react";

class UpDown extends Component{
    constructor(props){
        super(props);

        this.state = {
            count: 0
        }
    }

    onClickUpNum = ()=>{
        console.log("Down Number");
        this.setState({
            count: this.state.count +1
        })
    };

    onClickDownNum = ()=>{
        console.log("Up Number");
        this.setState({
            count: this.state.count -1
        })
    }
    render(){

        return(
            <div>
                <button onClick={this.onClickDownNum}> Down </button>
                <span> {this.state.count} </span>
                <button onClick={this.onClickUpNum}> Up </button>
            </div>
        )
    }
}

export default UpDown;